import random

string: str = 'Object'


class Animal:
    color = None
    weight = 0
    years = 0
    tail_length = 0
    current_speed = 0

    # def __init__(self):
    #     pass

    def __init__(self, color, weight=0, years=1, tail_length=1):
        self.color = color
        self.weight = weight

        if years <= 0:
            raise ValueError('Years must be greater then 0')

        self.years = years
        self.tail_length = tail_length

    def jump(self):
        print(f'jumped at {random.randint(0, 100)} meters.')

    def run(self):
        self.current_speed += 10
        print(f'running with speed {self.current_speed}')

    def run_and_jump(self):
        self.run()
        self.jump()

    def say(self, text):
        print(f'Animal says: {text}')

    def __del__(self):
        print('Animal die :(')

    def __str__(self):
        return f'{self.color} - {self.years}'


class Bird:

    def fly(self):
        print("I'm fly now!!!")

    def say(self, text):
        print(f'Chirik-chirik: {text}')


class Cat(Bird, Animal):
    name = None
    _protected = ''
    __secret = 'Secret data!'

    def __init__(self, name, color):
        self.name = name
        self.color = color

    def __del__(self):
        self._protected = 'ewqewqwew'
        print(f'Cat {self.name} die T_T')

    def jump(self):
        print('Cat jumps')

    def meow(self):
        self.__secret = 123123
        print('>(0_0)<')

    def run_and_jump(self):
        super().say('Test')
        self.meow()


# cat_1 = Cat(color='Black', name='Kiwi')
# cat_1._protected = 10
# print(cat_1._Cat__secret)

# cat_1.jump()
# cat_1.say('Hello')
# cat_1.run_and_jump()
# cat_1.say('Something')
# print(cat_1)

# animal1 = Animal(color='Black', years=10, tail_length=10)
# print(animal1)
# animal2 = Animal('Ginger')
#
# animal1.run()
#
# print(animal1.color)
# print(animal2.color)

# print(animal1.color, animal2.color)


# instance = MyClassName()
#
# MyClassName
