# with open('data_files/data.txt', 'r') as data:
#     file_data = data.read()
#     # print(data.read())
#     # print('-' * 20)
#     # if data.tell() != 0:
#     #     data.seek(0)
#     # print(data.read())
#
# file_data = [int(x) for x in file_data.split(',') if x.isdigit()]
# file_data.sort()
#
# with open('data_files/output.txt', 'w') as output:
#     output.write(','.join([str(x) for x in file_data]))


user_data = input('Enter a number: ')

try:
    with open('data_files/some.txt', 'w') as some:
        some.write('Test')

    user_data = float(user_data)
except ValueError:
    print('ERROR')
except ZeroDivisionError:
    print("Oh no some june broke my code!")
else:
    print('Yup!')
finally:
    print("Finally")

# raise Exception('RaccoonGang Best!')
