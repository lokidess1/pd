# items = ['hummer', 'shield', 'sword', 'bag']

# items = iter(items)

# for item in items:
#     print(item)

# print(next(items))
# print(next(items))
# print(next(items))
# print(next(items))

many_numbers = [1, 2, 3, 2, 1, 4, 3, 6, 3, 2, 7, 2]
odds = [x for x in many_numbers if x % 2 == 0]

# odds = [str(x) for x in many_numbers if x % 2 == 0]

for num in many_numbers:
    if num % 2 == 0:
        odds.append(num)

# print(', '.join([str(x) for x in many_numbers]))


counter = (x for x in range(2, 200000000000) if x % 2 == 0)

for item in counter:
    print(item)

