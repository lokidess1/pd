# import os
#
# path = os.path.join('etc', 'nginx')
#
# config_name = input("Enter config name: ")
#
# print(os.path.join(path, config_name))
#
# print(os.path.exists(os.path.join(path, config_name)))
#
# '/folder/dir/'  # - MAC OS, Linux, Android
# '\\folder\\dir\\'  # - Windows

# import datetime
# import re
#
#
# DATE_TIME_FORMAT = '%d-%m-%Y %H:%M'
# DATE_FORMAT_REGEXP = r'^\d{2}\-\d{2}\-\d{4} \d{2}:\d{2}$'
#
# while True:
#     first_date = input('Enter first date in format DD-MM-YYYY hh:mm: ')
#     second_date = input('Enter second date in format DD-MM-YYYY hh:mm: ')
#
#     if first_date == 'exit' or second_date == 'exit':
#         exit()
#
#     if re.match(DATE_FORMAT_REGEXP, first_date) and re.match(DATE_FORMAT_REGEXP, second_date):
#         diff = datetime.datetime.strptime(first_date, DATE_TIME_FORMAT) - \
#                datetime.datetime.strptime(second_date, DATE_TIME_FORMAT)
#
#         # import pdb; pdb.set_trace()
#         if diff.days < 0:
#             print(diff.days * -1)
#         else:
#             print(diff.days)
#     else:
#         print("Incorrect date format")
# import calendar
#
# print(calendar.calendar(2022))

# import time
#
# time.sleep(5)
# import pprint
# print = pprint.pprint
#
# lst_dct = [{'123': '123'}, {'asd': 1234123}, {'qwwds': 1231, 'sda': 123}]
#
# print(lst_dct, indent=4)


def ask_pwd(correct_pwd):

    def inner(function):

        def wrapper(*args, **kwargs):
            pwd = input('Enter password: ')
            if pwd == correct_pwd:
                result = function(*args, **kwargs)
                return result
            else:
                print('Access Deny')

        return wrapper
    return inner


@ask_pwd('3edc$RFV')
def some_secret_data():
    print("Here is my PH account password: ololololo")


@ask_pwd('5tgb^YHN')
def second_secret_data():
    print("Here is my FB account passord: olololololo")


some_secret_data()
second_secret_data()