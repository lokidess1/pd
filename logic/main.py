# lst = ['Loki', 16, "Dess"]

dct = {
    "name": "Loki",
    "age": 16,
    # "age": 22,
    "last_name": "Dess",
    "cats": [{
        "name": "Kiwi",
        "age": 4
    }, {
        "name": "Dolka",
        "age": 3
    }]
}

new_dct = {
    1: 'one',
    "first": 1,
    (1,2): 'O_O',
    True: 'True',
    None: "Omg",
    "name": 'Thor'
}

# KEY_VAR = "name"
#
# dct[KEY_VAR] = KEY_VAR
# dct['new_key'] = "new_data"

# some_new = dict.fromkeys(lst)

# print(new_dct["qweqweqwe"])
# print(new_dct.items())
# dct.update({
#     "new": 1,
#     "other": 2
# })
#
# dct["new"] = 1
# dct["other"] = 2

#
# print(dct['name'])

# lst = [1, 2, 4]
# lst.sort()
# 
# string = "124"
# upper = string.upper()
# 
# print(lst)

# user_number = int(input("Enter a number: "))
#
# allowed_nums = [1, 2, 3, 4, 5, 6, 7, 8]

# if user_number > 10 and user_number < 20 and user_number < 30:
#     print('Between 10 a 20')


# if user_number in allowed_nums:
#     print('Access granted!')
# else:
#     print('Access deny!')

# ==
# is

# print([1, 2] == [1, 2])
# print([] is [])

# a = 10000000000000000000000000000000000000000000
# b = 10000000000000000000000000000000000000000000
#
# print(1 == 1)
# print(a is b)

# a = [1, 2, 3]
#
# b = a
#
# print(a is b)

#
# if user_number > 10:
#     print("More then ten")
#
# elif user_number < 10:
#     print("Lower then ten")
#
# elif user_number > 20:
#     print("More then twenty!")
#
# else:
#     print("Equal!")
#
#
# if user_number > 10:
#     if user_number > 20:
#         if user_number > 30:
#             print('O_o')

# if 10 == 2:
#     pass
# else:
#     print('Not equal')
#
# if 10 != 2:
#     print('Not equal')
while True:
    user_data = input('Enter the number: ')

    if user_data.isdigit():
        user = int(user_data)
        break
    else:
        print("Enter a number you stupid beach!!!")
#
# print("End of work!")

# numbers[0] = numbers[0] + 1
# numbers[1] = numbers[1] + 1
# numbers[2] = numbers[2] + 1
# numbers[3] = numbers[3] + 1
# numbers[4] = numbers[4] + 1
# numbers[5] = numbers[5] + 1

counter = 0
numbers = [1, 2, 3, 4, 5, 6]

while counter < len(numbers):
    numbers[counter] = numbers[counter] + 1
    counter = counter + 1

#
# new_numbers = []
#
for item in range(0, 101):
    if item % 2 != 0:
        continue

    print(item)
#
# print(new_numbers)
