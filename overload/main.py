# TODO staticmethod

# class Table:
#     pass
#
#
# class Point:
#     x = 0
#     y = 0
#
#     def __init__(self, x, y):
#         self.x = x
#         self.y = y
#
#     @staticmethod
#     def draw(point, table):
#         print('Draw dot on table.')
#
#     def draw_this_point(self):
#         print(f'Draw point on cords: {self.x} {self.y}')


# Point.draw(Point(1, 10), Table())
# Point(2, 2).draw_this_point()
# print(Point.x, Point.y)

# TODO classmethod


# class Monster:
#     hp = 10
#     damage = 10
#     name = ''
#
#     def __init__(self, name):
#         self.name = name
#
#     @classmethod
#     def set_difficult(cls, difficult):
#         if difficult == 'medium':
#             cls.hp = 20
#             cls.damage = 20
#         elif difficult == 'hard':
#             cls.hp = 30
#             cls.damage = 30
#         elif 'hell':
#             cls.hp = 50
#             cls.damage = 50
#
#
# difficult = input('Select difficult: ')
#
# Monster.set_difficult(difficult)
#
# orc = Monster('Orc')
# pixi = Monster('Pixi')
# stepa = Monster('Stepa')
#
# print(orc.name, orc.hp, orc.damage)
# print(pixi.name, pixi.hp, pixi.damage)
# print(stepa.name, stepa.hp, stepa.damage)

# TODO property

# class Product:
#     _price = 0
#     name = ''
#     discount = 0
# 
#     def __init__(self, name, price, discount=0):
#         self.name = name
#         self._price = price
# 
#         if self.price > self.discount:
#             self.discount = discount
# 
#     @property
#     def price(self):
#         if self.discount > 0:
#             return self._price - self.discount
#         return self._price
# 
#     @price.setter
#     def price(self, value):
#         self._price = value


# product = Product('potato', 10, 3)
# product.price = 100
# print(product.price)


# TODO operator overload
import random


class Cat:
    name = ''
    gender = ''
    color = ''
    years = 0

    def __init__(self, name, gender, color):
        self.name = name
        self.color = color

        if gender in ['male', 'female']:
            self.gender = gender
        else:
            raise ValueError('Gender can be male or female')

    def __add__(self, other):
        if isinstance(other, Cat):
            if self.gender != other.gender:
                kitty_name = 'new_kitty'
                return Kitty(kitty_name, random.choice([self.color, other.color]))
            else:
                raise ValueError('Cats must be different genders')
        elif isinstance(other, int):
            self.years += other
            return self


class Kitty:
    name = ''
    color = ''
    years = 0

    def __init__(self, name, color):
        self.name = name
        self.color = color

    def __add__(self, other):
        self.years += other
        return self

    def __radd__(self, other):
        return self.__add__(other)


kiwi = Cat('Kiwi', 'male', 'black')
dolka = Cat('Dolka', 'female', 'smoke')
tor = Cat('Tor', 'male', 'black')

kiwi = kiwi + 1
kiwi += 1

kitty = kiwi + dolka

kitty = 1 + kitty

print(kitty.years)

# kitty_2 = dolka + tor
#
# print(kitty.name, kitty.color)
# print(kitty_2.name, kitty_2.color)
