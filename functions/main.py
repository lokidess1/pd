def is_float(data_str):
    value_to_test = data_str.replace(',', '.')

    if data_str.count('.') == 1:
        value_to_test = value_to_test.replace('.', '').replace('-', '')

        if value_to_test.isdigit():
            return True

    return False


# peoples = [
#     {'name': 'Loki', 'age': 16},
#     {'name': 'Thor', 'age': 123},
#     {'name': 'Tur', 'age': 56},
#     {'name': 'Eva', 'age': 63},
#     {'name': 'Magnus', 'age': 0}
# ]
#
# def get_age(item):
#     return item['age']
#
# peoples.sort(key=lambda x: x['age'])
# print(peoples)
# some = lambda x, y: x + y
#
# print(some(1, 1))

# is_float.some = 123
# print(is_float.some)

# user_input = input('Enter float: ')
#
# result = is_float(user_input)
#
# if result == True:
#     print('Valid')
# else:
#     print('Not Valid')


# def some_simple(first, second=0, third=None):
#
#     if third and is_float(third):
#         return first + second * float(third)
#
#     return first + second
#
#
# print(some_simple(1))


# def some(a=None):
#     # a = a or []
#     a.append(10)
#     print(a)
# #
# #
# print(some())  # [10]
# print(some())  # [10, 10]
# print(some())  # [10, 10, 10]


a = 300


def endless_args(*args, **kwargs):
    a = 10
    b = 20
    return a, b


some_data = [1, 3, 2, 3, 3, 2, 1]
some_dct_data = {'name': 'Loki', 'age': 16}

print(endless_args(*some_data, **some_dct_data))

# point = [1, 5]
#
# x, y = point
#
# print(x, y)

# a = 123
# b = 'qwe'
#
# a, b = b, a


# def log(msg):
#     with open('my.log', 'a') as log_file:
#         log_file.write(f'{msg}\n')
# 
# try:
#     1 / 0
# except ZeroDivisionError:
#     log('Zero division error, check the data')
# 
# 
# def endless_odd():
#     counter = 1
#     while True:
#         if counter % 2 == 0:
#             yield counter
#         counter += 1
# 
# 
# odds = endless_odd()
# 
# print(next(odds))
# print(next(odds))
# print(next(odds))
# print(next(odds))
# print(next(odds))
# print(next(odds))
# print(next(odds))


# for item in endless_odd():
#     print(item)
