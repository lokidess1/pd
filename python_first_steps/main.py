# this_is_snake_case
# thisIsCamelCase
# this-is-kebab-case
# ThisIsCasePascalCase

VERSION = '1.0'

name_of_user = 'Loki'
user_age = 16
user_data = 'Some'
float_num = 1.101010101
# true = True
# false = False

# print(VERSION, '-', name_of_user)

# name = input("Enter your name: ")
# print('Welcome ', name, '!')
# print(type(user_data))

# print(1.12345678901234567890)
# print(123+2j)

single_quotes = 'Some str'
double_quotes = "Some str"
multi_line_strings = '''
Some
Multi
'''

my_answer = "I can't tell you!"

first = 10
last = 13

result = first % 2

# help(print)
# print(result)
# my_str_value = "1.1"
# my_int_value = int(my_str_value)
# print(my_int_value)
# print(type(my_int_value))

print(int(1.9))
