name = 'Loki'

lst = [1, name, 3, 7, 'hello', 1.5, True, None]
#      0    1   2  3     4      5    6      7
# print(lst[6])

user = ['Loki', 16, 'loki@example.com']
user_odin = ['Odin', 'infinity', 'odin@example.com']

# array = [1, 2, 3, 4, 5]
matrix = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9]
]

# print(matrix[2][1][1][0])

new_lst = [1, 3, 9, 1, 6, 2, 8, 4, 6, 9, 3]

new_lst[0] = 12
# print(new_lst[-1])
# print(new_lst[-5:-1:])

# new_lst.append(7)
# print(new_lst)
# new_lst.insert(2, 123)
# print(new_lst)
# new_lst.remove(100)
# print(new_lst)

# print(new_lst.pop())
# print(new_lst)
#
# new_lst.sort()
# print(new_lst)

# a = [1, 2, 4]
# b = a
# a[1] = 12
# print(b)
# print(*new_lst)

# tpl = (1, 2, 3, 4)
# print(tpl[-1])
#
# new_tuple = (1, [1, 2, 3], 2)
# tpl[0] = 10
# new_tuple[1].append(4)
# print(new_tuple)

st = {1, 2, 3, 1, 3, 2}
st.pop()
print(st)

unique = list(set(new_lst))
print(unique)

rg = "Raccoon Gang!"

# print(rg.find('qwe'))
# print(rg.replace('a', ''))
# print(len(rg.split()))
# print(rg.count(' ') + 1)
# new_lst = [1, 2, 3]
# print(new_lst[len(new_lst) - 1])
# print(rg.upper())
# print(rg.lower())

first = "Loki"
last = "Dess"

print(first + " " + last)
print("Hello my name is {0} {1} ".format(first, last))
print(f"Hello my name is {first.upper()} {last}")
