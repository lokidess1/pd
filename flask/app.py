import csv
from flask import Flask, render_template, request, redirect

app = Flask(__name__)


@app.route('/')
def hello_world():
    return render_template('hello_world.html')


@app.route('/about/')
def about():
    return render_template('about.html')


@app.route('/feedback/', methods=['get', 'post'])
def feedback():
    if request.method == 'POST':
        name = request.form['name']
        feedback = request.form['feedback']
        age = request.form['age']

        if name and feedback and age:
            with open('feedback.csv', 'a') as feedback_file:
                row = f'"{name}","{age}","{feedback}"\n'
                feedback_file.write(row)
                return redirect('/thanks/')
        return render_template('error.html')

    return render_template('feedback.html')


@app.route('/thanks/')
def thanks():
    return render_template('thanks.html')


@app.route('/feedback/list/')
def feedback_list():

    with open('feedback.csv') as feedback_file:
        feedbacks = csv.reader(feedback_file, delimiter=',', quotechar='"')
        feedbacks = list(feedbacks)

    context = {
        'feedbacks': feedbacks
    }

    return render_template('feedback_list.html', **context)
