from typing import List

from pydantic import BaseModel, validator


class Feedback(BaseModel):
    name: str
    email: str
    age: int
    message: str

    @validator('name', 'email', 'message')
    @classmethod
    def not_empty_strings(cls, value: str):
        if not value:
            raise ValueError(f'{value} is not a string or empty')
        return value


class User(BaseModel):
    username: str
    email: str

    @validator('username')
    def not_empty_strings(cls, value: str):
        if not value:
            raise ValueError(f'{value} is not a string or empty')
        return value


class Tag(BaseModel):
    name: str


class Product(BaseModel):
    name: str
    price: float
    discount_price: float
    # user: User
    # tags: List[Tag]
