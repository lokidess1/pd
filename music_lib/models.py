from peewee import SqliteDatabase, Model, AutoField, CharField, IntegerField, TextField, ForeignKeyField

db = SqliteDatabase('music_lib.db')


class Author(Model):
    id = AutoField()
    name = CharField()
    year = IntegerField(null=True)
    about = TextField()

    class Meta:
        database = db
        table_name = 'author'


class Tag(Model):
    id = AutoField()
    name = CharField()

    class Meta:
        database = db
        table_name = 'tag'
