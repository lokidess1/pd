from flask import Flask, render_template, request, redirect
from models import Song, Author, Genre

app = Flask(__name__)


@app.route('/')
def index():
    context = {'songs': Song.select().order_by(-Song.name).where(Song.autor == 1)}
    return render_template('index.html', **context)


@app.route('/song/add/', methods=['get', 'post'])
def add_song():
    if request.method == 'GET':
        context = {
            'authors': Author.select(),
            'genres': Genre.select()
        }
        return render_template('add_song.html', **context)
    elif request.method == 'POST':
        Song.create(
            name=request.form['name'],
            autor=request.form['author'],
            genre=request.form['genre']
        )
        return redirect('/')
