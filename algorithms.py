# 1. Fib
# 2. find sum of k, [-9, -5, -1, 1, 4, 9, 11] k = 3 => [-1, 4]
# 3. Warmer distance [13, 12, 15, 11, 9, 12, 16] => [2, 1, 4, 2, 1, 1, 0]


# [13, 12, 15, 11, 9, 12, 16]
#  0   1   2   3   4  5   6
# []                         | (16, 6) | not | [(16, 6)] => 0
# [(16, 6)]                  | (12, 5) | yes | [(12, 5), (16, 6)] => 1
# [(12, 5), (16, 6)]         | (9,  4) | yes | [(9, 4), (12, 5), (16, 6)] => 1
# [(9, 4), (12, 5), (16, 6)] | (11, 3) | not | [(12, 5), (16, 6)] => ?
# [(12, 5), (16, 6)]         | (11, 3) | yes | [(11, 3), (12, 5), (16, 6)] => 2
# [(11, 3), (12, 5), (16, 6)]| (15, 2) | not | [(12, 5), (16, 6)] => ?
# [(12, 5), (16, 6)]         | (15, 2) | not | [(16, 6)] => ?
# [(16, 6)]                  | (15, 2) | yes | [(15, 2), (16, 6)] => 4
# [(15, 2), (16, 6)]         | (12, 1) | yes | [(12, 1), (15, 2), (16, 6)] => 1
# [(12, 1), (15, 2), (16, 6)]| (13, 0) | not | [(15, 2), (16, 6)] => ?
# [(15, 2), (16, 6)]         | (13, 0) | yes | [(13, 0), (15, 2), (16, 6)] => 2

def temperature(lst):  # O(log(n)) O(2n)
    answers = [0]
    counter = len(lst) - 2
    stack = [(lst[-1], counter + 1)]
    while counter >= 0:
        while stack:
            if lst[counter] < stack[0][0]:
                answers.insert(0, stack[0][1] - counter)
                stack.insert(0, (lst[counter], counter))
                break
            else:
                stack.pop(0)
        counter -= 1

    return answers

# def temperature(lst):  # O(n*n) O(n) | O(1)
#     answers = []
#     day_iter = 0
#     for day in lst:
#         next_day_iter = day_iter
#         for next_day in lst[day_iter:]:
#             if day < next_day:
#                 answers.append(next_day_iter - day_iter)
#                 break
#             next_day_iter += 1
#         day_iter += 1
#
#     answers.append(0)
#     return answers

print(temperature([13, 12, 15, 11, 9, 12, 16]))

# [-9, -5, -1, 1, 4, 9, 11]
# l=0 r=6 | -9 + 11 = 2 | < k
# l=1 r=6 | -5 + 11 = 6 | > k
# l=1 r=5 | -5 + 9 = 4  | > k
# l=1 r=4 | -5 + 4 = -1 | < k
# l=2 r=4 | -1 + 4 = 3  | = k


def find_sum(lst, value):  # O(n) O(1)
    left_pointer = 0
    right_pointer = len(lst) - 1

    while left_pointer < right_pointer:
        tmp = lst[left_pointer] + lst[right_pointer]

        if tmp == value:
            return [lst[left_pointer], lst[right_pointer]]

        if tmp < value:
            left_pointer += 1
        else:
            right_pointer -= 1

    return []

# def find_sum(lst, value):  # O(2n) O(1)
#     for item in lst:
#         tmp = value - item
#         if tmp in lst:
#             return [item, tmp]
#
#     return []


# print(find_sum([-9, -5, -1, 1, 4, 9, 11], 20))


# 2 2 4 6 10 16 26 ...

# def fib(number):  # O(n) O(1)
#     counter = 2
#     lst = [2, 2]
#
#     while counter < number:
#         lst.append(lst[0] + lst[1])
#         lst.pop(0)
#         counter += 1
#     return lst[-1]

#
# def fib(number, memo=None):  # O(n) O(2n)
#     memo = memo if memo is not None else {}
#
#     if number in memo:
#         return memo[number]
#
#     if number <= 2:
#         return 2
#
#     memo[number] = fib(number - 1, memo) + fib(number - 2, memo)
#
#     return memo[number]
#

# print(fib(1000))
