# class X:
#
#     def some(self):
#         pass
#
#
# class Y:
#
#     def some(self):
#         pass
#
#
# class Z:
#
#     def some(self):
#         pass
#
#
# class A(X, Y):
#
#     def some(self):
#         pass
#
#
# class B(Y, Z):
#
#     def some(self):
#         pass
#
#
# class M(A, B, Z):
#     pass
#
#
# print(M.__mro__)
# import random
#
#
# class Animal:
#     color = None
#     weight = 0
#     years = 0
#     tail_length = 0
#     current_speed = 0
#     name = ''
#     # def __init__(self):
#     #     pass
#
#     def __init__(self, color, name, weight=0, years=1, tail_length=1):
#         self.color = color
#         self.weight = weight
#         self.name = name
#
#         if years <= 0:
#             raise ValueError('Years must be greater then 0')
#
#         self.years = years
#         self.tail_length = tail_length
#
#     def jump(self):
#         print(f'jumped at {random.randint(0, 100)} meters.')
#
#     def run(self):
#         self.current_speed += 10
#         print(f'running with speed {self.current_speed}')
#
#     def run_and_jump(self):
#         self.run()
#         self.jump()
#
#     def say(self, text):
#         print(f'Animal says: {text}')
#
#     def __del__(self):
#         print('Animal die :(')
#
#     def __str__(self):
#         return f'{self.color} - {self.years}'
#
#
# class FlyAnimalMixin:
#
#     def fly(self):
#         print(f'{self.name} is fly now!')
#
#
# class SwimAnimalMixin:
#
#     def swim(self):
#         print(f'{self.name} can swim now')
#
#
# class Cat(FlyAnimalMixin, SwimAnimalMixin, Animal):
#     pass
#
#
# cat = Cat(color='black', name='Kiwi')
#
# cat.fly()
# cat.swim()
import random


class Monster:
    hp = 20
    name = ''

    def __init__(self, name):
        self.name = name

    def get_damage(self, damage):
        self.hp -= damage
        if self.hp <= 0:
            print('Monster dead.')


class Wizard:
    hp = 100
    mp = 100
    name = ''
    defence = 10

    def __init__(self, name):
        self.name = name

    # def __filter_func(self, method_name):
    #     method = getattr(self, method_name)
    #     return hasattr(method, 'alias') and method.alias == self.current_skill

    # def find_skill(self, skill_name):
    #     all_in_class = dir(self)
    #     self.current_skill = skill_name
    #     skills = filter(self.__filter_func, all_in_class)
    #     if skills:
    #         return getattr(self, list(skills)[0])

    def frost_bolt(self, monster):
        monster.get_damage(10)
        print(f'Frost bolt make 10 damage to {monster.name}')
    frost_bolt.alias = 'Frost Bolt'

    def fire_bolt(self, monster):
        monster.get_damage(15)
        print(f'Fire bolt make 10 damage to {monster.name}')
    fire_bolt.alias = 'Fire Bolt'

    def holy_nova(self, monster):
        monster.get_damage(20)
        print(f'Holy Nova make 10 damage to {monster.name}')
    holy_nova.alias = 'Holy Nova'


# player_name = input('Enter character name please: ')
player = Wizard(name='Loki')


getattr(player, 'hp')  # => player.hp


# prop_name = input('Enter property name: ')

# prop = getattr(player, prop_name, 'Unknown property')
# print(prop)

# while True:
#     command = input('Enter command: ')
#
#     if command == 'walk':
#         if random.randint(1, 2) == 1:
#             print('You find the monster')
#             monster = Monster(name='Orc')
#
#             while True:
#                 skill_name = input('Enter skill name: ')
#                 skill = getattr(player, skill_name, None)
#
#                 if skill is not None:
#                     skill(monster)
#                 else:
#                     print('Unknown Skill')
#
#                 if monster.hp <= 0:
#                     break
#         else:
#             print('Seams nothing here')
#     elif command == 'exit':
#         exit()
#     else:
#         print('Unknown command')

