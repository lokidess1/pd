def is_float(data_str):
    value_to_test = data_str.replace(',', '.')

    if data_str.count('.') == 1:
        value_to_test = value_to_test.replace('.', '').replace('-', '')

        if value_to_test.isdigit():
            return True

    return False


def slow_sort(iterable):
    result = []
    for _ in range(len(iterable)):

        lowest = iterable[0]
        for orig_item in iterable:
            if orig_item < lowest:
                lowest = orig_item

            iterable.remove(lowest)
        result.append(lowest)
    return result


def log(msg):
    with open('debug.log', 'a') as log_file:
        log_file.write(f'{msg}\n')
