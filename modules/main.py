# from basic_functions import log
# import basic_functions  # from basic_functions import *
# import basic_functions as bf

# from utils.secret.basic_functions import log as l
# from utils.secret.encripton import encript

# from .utils import log
#
#
# user_data = input('Enter some data: ')
#
# log(user_data)
# bf.log(user_data)
# bf.slow_sort([3,4,5,21])

# TODO REGEXP

import re

number_plate_regexp = r'[A-Z]{2}\d{4}[A-Z]{2}'

# print(re.match(r'^[A-Z]{2}\d{4}[A-Z]{2}$', "XA6325XA"))
# print(re.search(number_plate_regexp,
#                 "asdasghdfhgadsfasdgfjhXA6325XA7863827436287346"
#                 ).group()
#       )
# print(re.findall(number_plate_regexp,
#                  "asdasghdBI6377XAfhgadsfasdgfjhXA6325XA786382743628XA6325XA7346"
#                  )
#      )

print(re.sub(number_plate_regexp, '',
                 "asdasghdBI6377XAfhgadsfasdgfjhXA6325XA786382743628XA6325XA7346"
                 )
     )

number = "XA6325XA"
#         SSNNNNSS => ^[A-Z]{2}\d{4}[A-Z]{2}$

passport_number = "EE098892098"
#                  SSNNNNNNNNN => ^[А-ЯЇІҐ]{2}\d{9}$

town_name = "Kharkiv"
#            Sssssss+ => ^[A-Z]{1}[a-z]+?(\-[A-Z]{1}[a-z])$

phone = "+380992847333"
#        +38NNNNNNNNNN => ^?(\+38)\d{10}

