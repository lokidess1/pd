# program that check input

user_input = input("Enter a number: ").strip()

# 123.123, -123.123

value_to_test = user_input.replace(',', '.')

if user_input.count('.') == 1:
    value_to_test = value_to_test.replace('.', '').replace('-', '')

    if value_to_test.isdigit():
        print('Valid float')
    else:
        print('Error')
else:
    print('Error')

# splited = user_input.split('.')  # 0 - celoe, 1 - drobnoe
# is_length = len(splited) == 2
# is_digit = splited[0].isdigit() or (splited[0][0] == '-' and splited[0][1:-1].isdigit())
# 
# if is_length and is_digit and splited[1].isdigit():
#     print('Valid float')
# else:
#     print('ERROR')


# 123, -123

# if user_input.isdigit() or \
#         (user_input.startswith('-') and user_input.replace('-', '').isdigit()):
#     print('Valid INT')
# else:
#     print('ERROR')


# SOLUTION 2
# if user_input.isdigit() or (user_input[0] == '-' and user_input[1:-1].isdigit()):
#     print('Valid INT')
# else:
#     print('ERROR')


# SOLUTION 1
# all_digits = '1234567890-'
# is_error = False
#
# if len(user_input) > 0:
#
#     for letter in user_input:
#         if letter not in all_digits:
#             print('ERROR')
#             is_error = True
#             break
#
#     if not is_error:
#         print("Valid int")
# else:
#     print('EMPTY STRING')


