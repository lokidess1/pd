import time

from django import forms
from django.contrib.auth import login, authenticate
from django.contrib.auth.models import User
from django.core.mail import send_mail

from core.models import Feedback, Product


# class FeedbackForm(forms.Form):
#     name = forms.CharField()
#     email = forms.EmailField()
#     message = forms.CharField(widget=forms.widgets.Textarea(attrs={'cols': 30, 'rows': 5}))
#
#     def clean_name(self):
#         name = self.cleaned_data['name']
#         if not name.replace(' ', '').isalpha():
#             raise forms.ValidationError("Are you a king? NO NUMBERS!")
#         return name
#
#     def save(self):
#         Feedback.objects.create(
#             name=self.cleaned_data['name'],
#             email=self.cleaned_data['email'],
#             message=self.cleaned_data['message']
#         )
from core.tasks import send_feedback_mail


class FeedbackForm(forms.ModelForm):

    class Meta:
        model = Feedback
        fields = '__all__'


    def save(self, *args, **kwargs):
        obj = super(FeedbackForm, self).save(*args, **kwargs)
        send_feedback_mail.delay(self.cleaned_data['email'])
        return obj



class ProductForm(forms.ModelForm):

    class Meta:
        model = Product
        exclude = ('user', )

    def save(self, commit=True, user=None):
        product = super(ProductForm, self).save(commit=False)
        product.user = user
        product.save()
        self.save_m2m()
        return product


# class LoginForm(forms.Form):
#     username = forms.CharField()
#     password = forms.CharField(widget=forms.widgets.PasswordInput())
#
#     def __init__(self, *args, **kwargs):
#         self.request = kwargs.pop('request')
#         super(LoginForm, self).__init__(*args, **kwargs)
#
#     def clean_password(self):
#         user = authenticate(self.request, username=self.cleaned_data['username'], password=self.cleaned_data['password'])
#         if not user:
#             raise forms.ValidationError("Incorrect username or password")


class RegisterForm(forms.ModelForm):
    password_confirm = forms.CharField(widget=forms.widgets.PasswordInput())

    class Meta:
        model = User
        fields = ('username', 'email', 'password', 'password_confirm')
        widgets = {
            'password': forms.widgets.PasswordInput()
        }

    def clean_password_confirm(self):
        if self.cleaned_data['password'] != self.cleaned_data['password_confirm']:
            raise forms.ValidationError('Password do not match')
        return self.cleaned_data['password_confirm']

    def save(self, commit=True):
        user = super(RegisterForm, self).save(commit=False)
        user.set_password(self.cleaned_data['password'])
        user.save()
        self.save_m2m()
        return user


class ProductAdminForm(forms.ModelForm):

    class Meta:
        model = Product
        fields = '__all__'
        widgets = {
            'tags': forms.widgets.CheckboxSelectMultiple
        }
        # exclude = ('price', )
