from django.contrib.auth import authenticate
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.views import View
from django.views.generic import TemplateView, ListView, DetailView, FormView, CreateView, UpdateView

from api.serrializers import ProductSerializer
from core.forms import ProductForm, RegisterForm, FeedbackForm
from core.models import Product, Review, Feedback
from django.http import JsonResponse


# def index(request):
#     # request.GET  # - GET DATA from browser
#     # request.POST  # - POST DATA from browser
#     # request.FILES  # - FILES sended from browser
#     # return HttpResponse('Hello world')
#     return render(request, 'index.html')

class BaseIndexView(View):

    def dispatch(self, request, *args, **kwargs):
        return super(BaseIndexView, self).dispatch(request, *args, **kwargs)

    def some_my_method(self):
        # self.request
        pass

    def get(self, request):
        return render(request, 'index.html')

    def post(self, request):
        return render(request, 'index.html')


class IndexView(TemplateView):
    template_name = 'index.html'
    extra_context = {'page_title': 'Home'}

    def dispatch(self, request, *args, **kwargs):
        # if user logged in logic
        return super(IndexView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        context['products'] = Product.objects.all().select_related('user').prefetch_related('tags')[:300]
        return context

    def post(self, request):
        return render(request, 'index.html')


class AllProductView(ListView):
    template_name = 'all_products.html'
    queryset = Product.objects.all()
    context_object_name = 'products'


class SearchView(ListView):
    template_name = 'search.html'

    def get_queryset(self):
        # | - OR
        # & - AND
        # ~ - NOT
        filters = Q()

        if 'q' in self.request.GET:
            search_text = self.request.GET.get('q', ' ')
            filters.add(Q(name__contains=search_text) | Q(description__contains=search_text), Q.AND)

        if 'min-price' in self.request.GET and self.request.GET['min-price']:
            min_price = self.request.GET['min-price']
            filters.add(Q(price__gt=min_price), Q.AND)

        if 'max-price' in self.request.GET and self.request.GET['max-price']:
            max_price = self.request.GET['max-price']
            filters.add(Q(price__lt=max_price), Q.AND)

        if 'is_user_items' not in self.request.GET:
            filters.add(Q(user__isnull=True), Q.AND)

        return Product.objects.filter(filters).select_related('user').prefetch_related('tags')[:100]


class ProfileView(DetailView):
    template_name = 'profile.html'

    def get_object(self, queryset=None):
        return self.request.user


class CreateProduct(CreateView):
    template_name = 'create_product.html'
    model = Product
    form_class = ProductForm
    success_url = '/profile/'

    def form_valid(self, form):
        form.save(user=self.request.user)
        return redirect(self.success_url)


# class ProductEditView(TemplateView):
#     template_name = 'create_product.html'
#
#     def get_context_data(self, **kwargs):
#         context = super(ProductEditView, self).get_context_data(**kwargs)
#         product = Product.objects.get(id=self.kwargs['product_id'])
#         # context['form'] = ProductForm(initial={
#         #     'name': product.name,
#         #     'price': product.price,
#         #     'quantity': product.quantity
#         # })
#         context['form'] = ProductForm(instance=product)
#         return context
#
#     def post(self, request, product_id):
#         product = Product.objects.get(id=product_id)
#         form = ProductForm(instance=product, data=request.POST, files=request.FILES)
#         if form.is_valid():
#             form.save(user=request.user)
#             return redirect('/profile/')
#
#         context = self.get_context_data()
#         context['form'] = form
#         return self.render_to_response(context=context)


# class ProductEditView(FormView):
#     template_name = 'create_product.html'
#     form_class = ProductForm
#     success_url = '/profile/'
#
#     def form_valid(self, form):
#         form.save(user=self.request.user)
#         return super(ProductEditView, self).form_valid(form)
#
#     def get_form_kwargs(self):
#         product = Product.objects.get(id=self.kwargs['product_id'])
#         form_kwargs = super(ProductEditView, self).get_form_kwargs()
#         form_kwargs['instance'] = product
#         return form_kwargs


class ProductEditView(UpdateView):
    template_name = 'create_product.html'
    form_class = ProductForm
    pk_url_kwarg = 'product_id'
    success_url = '/profile/'
    queryset = Product.objects.all()

    def form_valid(self, form):
        form.save(user=self.request.user)
        return redirect(self.success_url)

# class FeedbackView(TemplateView):
#     template_name = 'feedback.html'
#
#     def get_context_data(self, **kwargs):
#         context = super(FeedbackView, self).get_context_data(**kwargs)
#         context['form'] = FeedbackForm()
#         return context
#
#     def post(self, request):
#         context = self.get_context_data()
#         form = FeedbackForm(data=request.POST)
#
#         if form.is_valid():
#             form.save()
#             return redirect('/feedback/')
#
#         context['form'] = form
#         return self.render_to_response(context)


# class FeedbackView(FormView):
#     template_name = 'feedback.html'
#     form_class = FeedbackForm
#     success_url = '/feedback/'
#
#     def form_valid(self, form):
#         form.save()
#         return super(FeedbackView, self).form_valid(form)


class FeedbackView(CreateView):
    template_name = 'feedback.html'
    model = Feedback
    form_class = FeedbackForm
    success_url = '/feedback/'


class ProductView(DetailView):
    template_name = 'product.html'
    model = Product
    pk_url_kwarg = 'product_id'

    def get_queryset(self):
        return Product.objects.filter(quantity__gt=0)

    # def get_object(self, queryset=None):
    #     return Product.objects.get(name=self.kwargs['product_id'])

    # def get_context_data(self, **kwargs):
    #     # self.kwargs['product_id']
    #     return super(ProductView, self).get_context_data(**kwargs)


# FormView, CreateView, UpdateView, DeleteView,
# class IndexView(TemplateView):
#     template_name = 'index.html'
#
#     def get(self, request, *args, **kwargs):
#
#         products = Product.objects.all()  # SELECT * FROM core_product;
#         products = Product.objects.filter(quantity=0)  # SELECT * FROM core_product WHERE quantity=0;
#         products = Product.objects.filter(quantity=0).order_by('name')  # SELECT * FROM core_product WHERE quantity=0 ORDER BY name;
#         # products = products.filter(quantity=0, price=9.99)  # SELECT * FROM core_product WHERE quantity=0 AND price=9.99
#         # products = products.filter(price=9.99)
#         mivina = Product.objects.get(name='Mivina')
#         all_mivina_review_1 = Review.objects.filter(product=mivina)
#
#         all_mivina_review_2 = Review.objects.filter(product__name='Mivina')
#
#         all_food_review = Review.objects.filter(product__tags__name='food')
#
#         available_products = Product.objects.filter(quantity__gt=0)  # WHERE quantity > 0;
#
#         print(all_mivina_review_1)
#         print(all_mivina_review_2)
#         print(all_food_review)
#         print(available_products)
#         return self.render_to_response(self.get_context_data())


# class LoginView(FormView):
#     template_name = 'login.html'
#     form_class = LoginForm
#     success_url = '/profile/'
#
#     def get_form_kwargs(self):
#         form_kwargs = super(LoginView, self).get_form_kwargs()
#         form_kwargs['request'] = self.request
#         return form_kwargs

class RegistrationView(FormView):
    template_name = 'registration.html'
    form_class = RegisterForm
    success_url = '/profile/'

    def form_valid(self, form):
        form.save()
        authenticate(self.request, username=form.cleaned_data['username'], password=form.cleaned_data['password'])
        return super(RegistrationView, self).form_valid(form)
