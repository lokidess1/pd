from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save, pre_save, post_delete, pre_delete, m2m_changed
from django.dispatch import receiver


class Tag(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class Product(models.Model):
    price = models.DecimalField(max_digits=10, decimal_places=2)
    quantity = models.IntegerField(default=0)
    name = models.CharField(max_length=255)
    description = models.TextField()
    cover_image = models.ImageField(upload_to='products', null=True, blank=True)
    tags = models.ManyToManyField(Tag)
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    edited_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        # PRE SAVE

        if self.pk:
            # UPDATE
            pass
        else:
            # CREATE
            from django.core.cache import cache
            cache.delete('index_page')

        instance = super(Product, self).save(*args, **kwargs)
        # POST SAVE
        return instance


@receiver(pre_save, sender=Product)
def after_product_add(sender, **kwargs):
    kwargs['instance'].name = "HAHAHAHAHAH"


class Review(models.Model):
    rating = models.FloatField()
    name = models.CharField(max_length=255)
    message = models.TextField(null=True, blank=True)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)

    def __str__(self):
        return f'Product: {self.product} - Review from: {self.name}'


class Feedback(models.Model):
    name = models.CharField(max_length=255)
    email = models.EmailField()
    message = models.TextField()
    
    def __str__(self):
        return f'{self.name} - {self.email}'

