from django import template

from core.models import Product

register = template.Library()


@register.filter
def multiple(first, second):
    # first - left side of filter
    # second - after :
    return first * second


@register.simple_tag
def some_interesting():
    return "Hello world"


@register.inclusion_tag('includes/random_products.html')
def random_products(product_count=4):
    return {
        'random_products': Product.objects.all().order_by('?')[:product_count]
    }
