from django.contrib import admin
from django.template.defaultfilters import safe

from core.forms import ProductAdminForm
from core.models import Tag, Product, Review, Feedback


class ReviewInline(admin.TabularInline):
    model = Review
    extra = 0


class ProductAdmin(admin.ModelAdmin):
    list_display = ('name', 'price', 'quantity', 'user', 'product_link')
    search_fields = ('name', 'description')
    list_filter = ('user', 'tags')
    list_editable = ('quantity', )
    form = ProductAdminForm
    inlines = (ReviewInline, )
    actions = ('no_longer_available', )
    # readonly_fields = ('price', )

    def get_queryset(self, request):
        queryset = super(ProductAdmin, self).get_queryset(request)
        queryset = queryset.select_related('user').prefetch_related('tags')
        return queryset

    def product_link(self, obj):
        return safe(f"<a href='/product/{obj.id}/' target='_blank' >Show in shop</a>")
    product_link.short_description = ''

    def no_longer_available(self, request, queryset):
        queryset.update(quantity=0)
    no_longer_available.short_description = 'Make quantity 0'


class TagAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', )
    list_editable = ('name', )


admin.site.register(Tag, TagAdmin)
admin.site.register(Product, ProductAdmin)
admin.site.register(Review)
admin.site.register(Feedback)
