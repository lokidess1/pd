from core.models import Tag


def global_tags(request):
    return {
        'tags': Tag.objects.all()
    }
