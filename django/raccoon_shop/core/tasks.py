# import datetime
import time

from celery import shared_task
from django.contrib.auth.models import User
from django.core.mail import send_mail
from django.utils import timezone

from core.models import Product


@shared_task
def send_feedback_mail(email):
    time.sleep(10)
    send_mail(
        subject='Feedback leave success.',
        message='Test email',
        from_email='raccoon_shop@example.com',
        recipient_list=[email]
    )

@shared_task
def send_promo_emails():
    users = User.objects.all().values_list('email', flat=True)
    today = timezone.now()
    products = Product.objects.filter(
        created_at__day=today.day,
        created_at__month=today.month,
        created_at__year=today.year
    )

    message = f'Hey! Checkout our new products: {", ".join([f"{x.name} - {x.price}" for x in products])}'

    for email in users:
        send_mail(
            subject='New income',
            message=message,
            from_email='raccoon_shop@example.com',
            recipient_list=[email]
        )
