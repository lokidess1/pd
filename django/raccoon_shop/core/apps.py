from django.apps import AppConfig
from suit.apps import DjangoSuitConfig


class CoreConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'core'


class SuitConfig(DjangoSuitConfig):
    layout = 'horizontal'
