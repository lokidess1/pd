from django.contrib.auth.models import User
from rest_framework import serializers

from core.models import Product, Tag


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('username', 'id')


class TagsSerializer(serializers.ModelSerializer):

    class Meta:
        model = Tag
        fields = '__all__'


class ProductSerializer(serializers.ModelSerializer):
    tags = TagsSerializer(many=True, read_only=True)
    tag_ids = serializers.ListField(write_only=True)
    user = UserSerializer(read_only=True)
    user_id = serializers.IntegerField(write_only=True)
    total_cost = serializers.SerializerMethodField()
    current_user = serializers.SerializerMethodField()
    cover_image = serializers.ImageField(read_only=True)

    class Meta:
        model = Product
        fields = '__all__'

    def get_total_cost(self, obj):
        return obj.price * obj.quantity

    def get_current_user(self, obj):
        user = self.context['request'].user
        return UserSerializer(user).data

    def create(self, validated_data):
        tag_ids = validated_data.pop('tag_ids')
        product = super(ProductSerializer, self).create(validated_data)
        tags = Tag.objects.filter(id__in=tag_ids)
        product.tags.add(*tags)
        return product

    # def validate_user_id(self):
