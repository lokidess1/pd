from rest_framework import generics, viewsets
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.throttling import UserRateThrottle, SimpleRateThrottle
from rest_framework.views import APIView

from api.serrializers import ProductSerializer
from core.models import Product


class ProductsThrottle(SimpleRateThrottle):
    scope = 'products'

    def get_cache_key(self, request, view):
        return self.cache_format % {
            'scope': self.scope,
            'ident': self.get_ident(request)
        }

# class ProductList(APIView):
#
#     permission_classes = (IsAuthenticated, )
#     # authentication_classes = ()
#
#     def get(self, request):
#         products = Product.objects.all()
#         serialized = ProductSerializer(products, many=True, context={'user': request.user})
#         return Response(serialized.data)

# viewsets.ModelViewSet

class ProductApiView(generics.ListAPIView, generics.RetrieveAPIView, viewsets.GenericViewSet):
    serializer_class = ProductSerializer
    queryset = Product.objects.all()
    throttle_classes = (ProductsThrottle, )

    # def get_queryset(self):

class ProductApiViewCUD(generics.CreateAPIView, generics.UpdateAPIView, generics.DestroyAPIView, viewsets.GenericViewSet):
    serializer_class = ProductSerializer
    queryset = Product.objects.all()
    permission_classes = (IsAuthenticated,)
    authentication_classes = (TokenAuthentication, )
    throttle_classes = (UserRateThrottle, )
