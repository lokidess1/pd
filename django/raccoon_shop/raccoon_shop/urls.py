"""raccoon_shop URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.contrib.auth.views import LoginView, LogoutView
from django.urls import path, include
from core import views
from api import views as api_views
from django.views.decorators.cache import cache_page

from rest_framework import routers

router = routers.DefaultRouter()
router.register('products', api_views.ProductApiView)
router.register('product', api_views.ProductApiViewCUD)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.IndexView.as_view()),
    path('products/', views.AllProductView.as_view()),
    path('search/', views.SearchView.as_view()),
    path('product/<str:product_id>/', views.ProductView.as_view()),
    path('feedback/', views.FeedbackView.as_view()),
    path('profile/', views.ProfileView.as_view()),
    path('create_product/', views.CreateProduct.as_view()),
    path('edit_product/<int:product_id>/', views.ProductEditView.as_view()),
    path('login/', LoginView.as_view(template_name='login.html')),
    path('logout/', LogoutView.as_view()),
    path('register/', views.RegistrationView.as_view()),
    path('api-auth/', include('rest_framework.urls')),
    path("__debug__/", include("debug_toolbar.urls")),
    path('api/v1/', include(router.urls))
    # path('', views.index)
    # path("api/v1/products/", api_views.ProductList.as_view())
]

