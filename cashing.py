import random
import time
import pickle
import os
from functools import lru_cache


def cache(function):

    def wrapper(*args, **kwargs):
        file_path = f'cache/{function.__name__}'

        if not os.path.exists(file_path):
            cached_values = {}
        else:
            with open(file_path, 'rb') as cache_file:
                cached_values = pickle.load(cache_file)

        if args in cached_values:
            return cached_values[args]
        else:
            cached_values[args] = function(*args, **kwargs)
            with open(file_path, 'wb') as cache_file:
                pickle.dump(cached_values, cache_file)

            return cached_values[args]

    return wrapper


@lru_cache(maxsize=128)
def sum(first, last):
    time.sleep(3)
    return first + last


def sum_r(first, last):
    return first + last + random.randint(0, 10)


if __name__ == '__main__':
    # cache = {}
    counter = 0
    while counter < 50:
        print(sum(random.randint(0, 5), random.randint(0, 5)))
        counter += 1
